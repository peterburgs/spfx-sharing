# SPFX Sharing

<sub>Author: Peter Burgs.</sub>
<sub>Last updated: Jan 2, 2023</sub>

<hr>

## Description

<p style="text-align: justify">
When working with SPFX projects, we would face varied problems related to flaws. This documentation is not 100% perfect but might be a reliable source for those need a starter mindset 🚀 
</p>

<br />

<p style="text-align: justify">The following conventions is supposed to be applied for TypeScript or any other typed-coupling programing languages.</p>

<hr>

## Agenda

<ol>
<li>
<details>
<summary>🍕 Coding Conventions</summary>

<ul>
<li>
<details>
<summary>
Identifiers
</summary>
- Identifiers must use only Alphabet letters, digits, underscores

- Class / Interface / Type / Enum: <b>UpperCamelCase</b>
- variable / parameter / function / method: <b>lowerCamelCase</b>
- global constant values: <b>CONSTANT_CASE</b>
</details>
</li>

<li>
<details>
<summary>Naming</summary>
<h4 style="color: green">- Use meaningful variable names 🙈 🙉 🙊</h4>
<p> Distinguish names in such a way that the reader knows what the differences offer.</p>

```ts
function isBetween(a1: number, a2: number, a3: number): boolean {
	return a2 <= a1 && a1 <= a3;
}
```

```ts
function isBetween(value: number, left: number, right: number): boolean {
	return left <= value && value <= right;
}
```

<hr>

<h4 style="color: green">- Use pronounceable variable names 🙈 🙉 🙊</h4>
<p> If you can't pronounce it, you can't discuss it without sounding weird.</p>

```ts
class Subscription {
	public creditCardId: number;
	public billingAddressId: number;
	public shippingAddressId: number;
}
```

```ts
class Subs {
	public ccId: number;
	public billingAddrId: number;
	public shippingAddrId: number;
}
```

<hr>

<h4 style="color: green">- Avoid mental mapping 🙈 🙉 🙊</h4>
<p>Explicit is better than implicit.</p>

```ts
const u = getUser();
const s = getSubscription();
const t = charge(u, s);
```

```ts
const user = getUser();
const subscription = getSubscription();
const transaction = charge(user, subscription);
```

<hr>

<h4 style="color: green">- Don't add unneeded context 🙈 🙉 🙊</h4>
<p>If your class/type/object name tells you something, don't repeat that in your variable name.</p>

```ts
type Car = {
	carMake: string;
	carModel: string;
	carColor: string;
};

function print(car: Car): void {
	console.log(`${car.carMake} ${car.carModel} (${car.carColor})`);
}
```

```ts
type Car = {
	make: string;
	model: string;
	color: string;
};

function print(car: Car): void {
	console.log(`${car.make} ${car.model} (${car.color})`);
}
```

<hr>

</details>
</li>

<li>
<details>
<summary>Typing</summary>
<img src="https://www.fortressofsolitude.co.za/wp-content/uploads/2021/10/Best-Spider-Man-Memes.png">

- Should add types to all variables, functions.
- Only use <b>any</b> for error.
- Common types/interfaces should be located in Typing folders.
- Specific types/interfaces can be defined within the files.

[How much time do you spend writing code vs reading code?](https://www.quora.com/How-much-time-do-you-spend-writing-code-vs-reading-code)
</detail>

</li>
</ul>

</details>

</li>

<li>
<details>
<summary>💼 Git Conventions</summary>

#### 1. Features

- Main features must be checked out from branch `dev.`
- Sub features can be checked out from `Main feature's` branch or branch `dev.`
- Feature branch name must follow the format: `feature/[ticket_name]`

Branch name example: `feature/MS-100`

#### 2. Modules

- A module contains many features.
- The last commit of a module should be tagged before releasing the module.
- Tagging version format: `[x].[y].[z]`
  where: x is a huge update, y is a medium changes, z is small modification.
  Ref: [Git Tagging](https://git-scm.com/book/en/v2/Git-Basics-Tagging)

#### 3. Master

- Only branch `dev` can be merged to `master.`
- Only repository owner can merge `dev` to `master.`
- If any bugs happen on `master`, must create a `hotfix` immediately.
- Only check out `master` for releasing.

#### 4. Hotfix

- To fix bug on `master` or `release.`
- Hotfix branch name must follow the format: `hotfix/[ticket_name]`

Hotfix branch name example: `hotfix/MS-100`

#### 5. Rebase

- Must rebase before pushing new codes to remote.
- Never use `merge` command.
  -Ref: [Merging vs. rebasing](https://www.atlassian.com/git/tutorials/merging-vs-rebasing#:~:text=Merging%20is%20a%20safe%20option,onto%20the%20tip%20of%20main%20.)

#### 6. Resolve conflict

- Contact commit author before resolving any conflicts.
<hr>

## Semantic Commit Messages

See how a minor change to your commit message style can make you a better programmer.

Format: `<Task> | <Summary> <Description>`

`<Summary>` is optional

### Example

```
MS-17 | Set up MongoDB on Local environment  --> Summary
- Create Docker container for MongoDB       --> Description
- Validate connection via MongoDB Compass   --> Description
- Update README                             --> Description
```

References:

- https://www.conventionalcommits.org/
- https://seesparkbox.com/foundry/semantic_commit_messages
- http://karma-runner.github.io/1.0/dev/git-commit-msg.html

</details>

</li>

<li>
<details>
<summary>👨‍🦯 Documentation & Maintain Documents</summary>
A project should have readable document:

- README.md
- Database schema
- System design diagram
  ...

<hr>

A good document should contains:

- Environment requirements.
- Installation steps.
- Start local environment.

</details>
</li>
</ol>

<details>
<summary>[Optional]:  Patterns and Practices for SPFx Development</summary>

### High Level Solution Structure

```bash
+-- src
|   +-- common
|   +-- controls
|   +-- helpers
|   +-- hooks
|   +-- models
|   +-- services
|       +-- business
|       +-- dataAccess
|   +-- webparts
+-- teams
```

### UI Guidelines

- If you’re building rich complex UI and if you have identified a large set of UI components, you can probably create separate folders to group them.

- These UI components should only contain rendering logic. Do not add any business logic or data access code in these components.

- You should not add multiple function components/classes to single file.

- Consider keeping the code minimal in render method.

- Consider handling exception properly. Log the exception details with logger components and present generic informative message for end-user on screen.

[Read here for more details](https://pnp.github.io/blog/post/patterns-and-practices-for-spfx-development/)

</details>

<hr>

## Motto

Don't make it just work, make it maintainable
